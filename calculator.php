<!doctype html>
<html>
<head>
    <title>Calculator</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="styles.css" />  
</head>
<body>
    <h1 id="header">The Amazing Calculator</h1>
    <div id="content">
        <form action="calculator.php" method="GET">
            First number: <input type="number" name="first_var" /><br>
            <input type="radio" name="operation" value="multiplication" checked="checked"/>x<br>
            <input type="radio" name="operation" value="division"/>/<br>
            <input type="radio" name="operation" value="addition"/>+<br>
            <input type="radio" name="operation" value="subtraction"/>-<br>
            Second number: <input type="number" name="second_var"/>
            <input type="submit" value="Submit" id="submitButton" /><br>
            <h4>Result:</h4>
        </form>
        <?php
            if(isset($_GET['first_var']) AND isset($_GET['operation']) AND isset($_GET['second_var'])) {
                $firstvar = $_GET['first_var'];
                $operation = $_GET['operation'];
                $secondvar = $_GET['second_var'];
                $answer;
                switch($operation) {
                    case "multiplication":
                        $answer = $firstvar * $secondvar;
                        break;
                    case "division":
                        $answer = $firstvar / $secondvar;
                        break;
                    case "addition":
                        $answer = $firstvar + $secondvar;
                        break;
                    case "subtraction":
                        $answer = $firstvar - $secondvar;
                        break;
                    default: break;
                }
                echo "$answer";
            }
        ?>
    </div>
</body>
</html>